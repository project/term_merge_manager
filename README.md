# Term Merge Manager

## Contents of this file

- Introduction
- Requirements
- Installation
- Maintainers

## Introduction

This module extends the Term Merge Module.

It saves all Term Merge Actions and automatically reapply them on new terms.

For example:  
You merge "foo" and "bar" into "foobar".  
The next time the term "foo" is tried to be created on the same vocabulary,   
it's automatically changed into "foobar".

- For a full description of the module, visit the [project page](https://www.drupal.org/project/term_merge_manager).
- Use the [Issue queue](https://www.drupal.org/project/issues/term_merge_manager) to submit bug reports and feature suggestions, or track changes.

## Requirements

- Requires [Term Merge](https://www.drupal.org/project/term_merge)

## Installation

- Install as you would normally install a contributed Drupal module. See [installing modules](https://www.drupal.org/node/895232) for further information.

## Maintainers

Current maintainers:
* Matthias Froschmeier ([@mfrosch](https://www.drupal.org/u/mfrosch))

